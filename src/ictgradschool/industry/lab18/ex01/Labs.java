package ictgradschool.industry.lab18.ex01;

/**
 * Created by jwon117 on 16/05/2017.
 */
public class Labs extends Marks {

    public Labs(int studentSkill) {
        super(studentSkill);
        this.abilityB = 65;
        this.abilityC = 25;
        this.abilityD = 15;
        this.abilityE = 5;
    }
}
