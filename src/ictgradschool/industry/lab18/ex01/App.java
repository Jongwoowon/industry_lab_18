package ictgradschool.industry.lab18.ex01;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jwon117 on 16/05/2017.
 */
public class App {

    public static void main(String[] args) {

        List<Students>studentList = new ArrayList<>();
        int classSize = 550;

        StudentInfoReader firstnameRead = new StudentInfoReader(new File("FirstNames.txt"));
        StudentInfoReader surnameRead = new StudentInfoReader(new File("Surnames.txt"));

        List<String> firstnameList = firstnameRead.getStudentList();
        List<String> surnameList = surnameRead.getStudentList();

        StudentInfoWriter writer = new StudentInfoWriter(new File("Data_Out.txt"));

        for (int i = 0; i <classSize; i++) {
            Students newStudent = new Students(firstnameList.get((int)(Math.random()*firstnameList.size())), surnameList.get((int)(Math.random()*surnameList.size())));
            newStudent.setLabMark(new Labs(newStudent.getSkillSet()).generateMark(newStudent.getSkillSet()));
            newStudent.setTestMark(new Test(newStudent.getSkillSet()).generateMark(newStudent.getSkillSet()));
            newStudent.setExamMark(new Exam(newStudent.getSkillSet()).generateMark(newStudent.getSkillSet()));
            System.out.println(newStudent);
            studentList.add(newStudent);
        }

        writer.StudentRecords(studentList);
    }

}
