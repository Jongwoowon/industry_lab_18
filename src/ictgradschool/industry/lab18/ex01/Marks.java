package ictgradschool.industry.lab18.ex01;

/**
 * Created by jwon117 on 16/05/2017.
 */
public class Marks {

    protected int studentSkill;
    protected int abilityB;
    protected int abilityC;
    protected int abilityD;
    protected int abilityE;


    public Marks(int studentSkill) {
        this.studentSkill = studentSkill;
    }

    public int generateMark(int studentSkill) {

        if (studentSkill <= abilityE) {
            return markComponent(); //[0,39]
        } else if ((studentSkill > abilityE) && (studentSkill <= abilityD)) {
            return markComponent(10, 40); // [40,49]
        } else if ((studentSkill > abilityD) && (studentSkill <= abilityC)) {
            return markComponent(20, 50); // [50,69]
        } else if ((studentSkill > abilityC) && (studentSkill <= abilityB)) {
            return markComponent(20, 70); // [70,89]
        } else {
            return markComponent(11, 90); //[90,100]
        }
    }
    public int markComponent(int range, int base_line) {
        return (int) (Math.random() * range) + base_line;
    }

    public int markComponent() {
        return (int) (Math.random() * 40);
    }

}
