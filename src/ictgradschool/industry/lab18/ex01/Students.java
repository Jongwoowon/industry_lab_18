package ictgradschool.industry.lab18.ex01;

/**
 * Created by jwon117 on 16/05/2017.
 */
public class Students {

    private String firstName;
    private String lastName;
    private int labMark;
    private int testMark;
    private int examMark;
    private int skillSet;

    public Students(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.skillSet = (int)(Math.random()*101);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getSkillSet() {
        return skillSet;
    }

    public void setLabMark(int labMark) {
        this.labMark = labMark;
    }

    public void setTestMark(int testMark) {
        this.testMark = testMark;
    }

    public void setExamMark(int examMark) {
        this.examMark = examMark;
    }

    @Override
    public String toString() {
        return "Students{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", labMark=" + labMark +
                ", testMark=" + testMark +
                ", examMark=" + examMark +
                ", skillSet=" + skillSet +
                '}' + "\n";
    }
}
