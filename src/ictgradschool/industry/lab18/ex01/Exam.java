package ictgradschool.industry.lab18.ex01;

/**
 * Created by jwon117 on 16/05/2017.
 */
public class Exam extends Marks {
    public Exam(int studentSkill) {
        super(studentSkill);
        this.abilityB = 90;
        this.abilityC = 60;
        this.abilityD = 20;
        this.abilityE = 7;
    }

    @Override
    public int markComponent() {
        int randDNSProb = (int)(Math.random()*101);
        if(randDNSProb <= 5){
            return -1; //DNS
        }else{
            return (int)(Math.random()*40); //[0,39]
        }
    }

    public static void main(String[] args) {
        Exam testing = new Exam(7);
        System.out.println(testing.generateMark(testing.studentSkill));
    }
}
