package ictgradschool.industry.lab18.ex01;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jwon117 on 16/05/2017.
 */
public class StudentInfoReader {

    private File thisFile;

    public StudentInfoReader(File thisFile) {
        this.thisFile = thisFile;
    }

    public List<String> getStudentList() {

        ArrayList<String> nameList = new ArrayList<>();

        try {
            BufferedReader br = new BufferedReader(new FileReader(thisFile));
            String lah;
            while ((lah = br.readLine()) != null) {
                nameList.add(lah);
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return nameList;
    }
}