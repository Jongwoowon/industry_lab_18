package ictgradschool.industry.lab18.ex01;

/**
 * Created by jwon117 on 16/05/2017.
 */
public class Test extends Marks {
    public Test(int studentSkill) {
        super(studentSkill);
        this.abilityB = 90;
        this.abilityC = 65;
        this.abilityD = 20;
        this.abilityE = 5;
    }
}
